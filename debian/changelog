tuareg-mode (1:2.1.0-3) UNRELEASED; urgency=medium

  * Convert to dh-elpa (Closes: #905940):
    - Binary package must be named elpa-tuareg for compatibility
      with MELPA.
    - Drop legacy debian/*emacsen* scripts.
    - Install LISP files using debian/elpa-tuareg.elpa
  * d/control:
    - Add dh-elpa build-dep.
    - Enable autopkgtest-pkg-elpa.
    - Elpa-tuareg contains the elpafied package and tuareg-mode
      is now a dummy package.
    - Add Breaks and Replaces against old tuareg-mode.
    - Rely on dh-elpa for elpa-tuareg's Depends.
    - Drop XEmacs from long description because dh-elpa does not support it.
      (Closes: #850071, #582981)
    - Add Enhances emacs, which is conventional for elpa packages.
  * Rename debian/tuareg-mode.docs -> debian/elpa-tuareg.docs
  * Rewrite d/rules to use dh-elpa, and ignore upstream Makefile.
  * d/changelog: add myself, because this number of changes requires it.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 22 Jan 2019 14:23:13 -0700

tuareg-mode (1:2.1.0-2) unstable; urgency=medium

  * Update Vcs-* to salsa
  * Standards-Version 4.2.0 (no change)
  * d/copyright: https in url of format specification
  * debhelper compatibility level 11 (no change)
  * as-installed test (closes: #906226)
    - depend on emacs
    - test with tuareg.el instead of tuareg.elc (to be undone when the
      package is converted to dh-elpa)

 -- Ralf Treinen <treinen@debian.org>  Wed, 15 Aug 2018 18:22:37 +0200

tuareg-mode (1:2.1.0-1) unstable; urgency=medium

  * New upstream version
    - refresh patch ocaml-path
    - drop patch opam, issue is resolved by upstream
  * debian/gbp.conf: drop field cleaner
  * debhelper compat level 10
  * Standards-Version 4.1.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 22 Nov 2017 21:24:45 +0100

tuareg-mode (1:2.0.10-1) unstable; urgency=medium

  * new upstream release.
  * emacsen-startup: autoload tuareg-run-ocaml. Thanks to Sameer Bagwhat
    for the patch (closes: #522058)
  * patch opam: set tuareg-interactive-program to opam only when opam is
    installed, and the user has an .opam directory (closes: #788456)
  * emacsen-startup: activate SMIE, since issues seem to be fixed.
  * d/rules: install all tuareg*.el files
  * standards-version 3.9.8 (no  change)
  * d/control: dropped depricated XS-Testsuite
  * d/control: updated Vcs-Git and Vcs-Browser
  * d/control: capitalisation in short description
  * d/control: updated Homepage

 -- Ralf Treinen <treinen@debian.org>  Sun, 17 Jul 2016 21:15:00 +0200

tuareg-mode (1:2.0.9-2) unstable; urgency=low

  * debian/control: fix url in Vcs fields.

 -- Ralf Treinen <treinen@debian.org>  Tue, 05 May 2015 18:20:03 +0200

tuareg-mode (1:2.0.9-1) experimental; urgency=medium

  * New upstream release. This release fixes a bad interaction with
    show-paren-mode (closes: #780805)
  * debian/rules:
    - drop override of dh_installchangelogs
    - install *.el files newly added by upstream
    - drop spurious .PHONY declarations for dh override targets
    - drop variable export TUAREGXELDIR
  * debian/tuareg-mode.docs: README is now README.md
  * debian/watch: switch to new upstream site at github.
  * Standards-version 3.9.6 (no change)
  * add DEP8-style package test

 -- Ralf Treinen <treinen@debian.org>  Sun, 22 Mar 2015 15:46:28 +0100

tuareg-mode (1:2.0.7-1) unstable; urgency=low

  [ Sylvain Le Gall]
  * Remove Sylvain Le Gall from uploaders

  [ Ralf Treinen ]
  * New upstream release. This release fixes
    - some bugs with syntax highliting and indentation (closes: #207471)
    - incorrect indentation of nested if statements (closes: #617407)
  * change dependency on emacs23 to emacs (closes: #754032)
  * Standards-Version 3.9.5 (no change)
  * Drop conflict with ocaml-tools (<=1.1-2) which is no longer distributed
    since october 2001.
  * Drop patch emacs21 which is no longer relevant - we don't care for
    compatibility with emacs version 21 any more.
  * Added debian/emacsen-compat, added dependency on emacsen-common >= 2.0.8

 -- Ralf Treinen <treinen@debian.org>  Thu, 10 Jul 2014 10:31:58 +0200

tuareg-mode (1:2.0.6-3) unstable; urgency=low

  * Updated the emacsen-install file from the current template shipped
    with dh-make to fix an upgrade failure from squeeze (closes: #681823).
    Thanks to Hilko Bengen for the patch!

 -- Ralf Treinen <treinen@debian.org>  Sat, 28 Jul 2012 14:16:52 +0200

tuareg-mode (1:2.0.6-2) unstable; urgency=low

  * debian/emacsen-install: fix tuareg-use-smie (f -> nil). Thanks to
    Kevin Ryde (closes: #676860).
  * debian control: first choice of emacsen is emacs23, not xemacs21
  * new patch emacs21: add an (required 'derived) in case it is not loaded,
    needed by emacs21. Thanks to Kevin Ryde for the patch (closes: #676858).

 -- Ralf Treinen <treinen@debian.org>  Tue, 19 Jun 2012 09:46:23 +0200

tuareg-mode (1:2.0.6-1) unstable; urgency=low

  * New upstream version.
  * debian/watch: also accept .tar.gz files
  * upstream does noy longer distribute append-tuareg.el. Put our own version.
    Drop patches to append-tuareg.el that are hence no longer necessary:
     0001-camldebug-of-tuareg-mode-is-renamed-to-camldebug-tua.patch
     0003-narrow-down-auto-alist
  * Drop patch 0002-Use-locally-installed-documentation which is no longer
    relevant as we do not have any local documentation
  * Add patch ocaml-path to fix the ocaml library path according to the
    debian ocaml policy.
  * debian/rules: no more files custom-tuareg.el or sym-lock.el to install
  * installation: do not compile for xemacs.
  * debian/emacsen-install: create in ELC directory symbolic links for the
    corresponding .el files (closes: #671573)
  * debian/copyright: remove mention of ocamlspot.el which is no longer part
    of the upstream distribution.

 -- Ralf Treinen <treinen@debian.org>  Wed, 06 Jun 2012 22:00:20 +0200

tuareg-mode (1:2.0.4-4) unstable; urgency=low

  * Bring files tuareg.el and append-tuareg.el to upstream version 2.0.4.
    These two files were left in version 2.0.1 due to an accident.
  * Patch 0003-narrow-down-auto-alist: regexp must apply only at the end
    of a filename. Thanks to Kevin Ryde for the patch (closes: #671558).
  * debian/control: drop starting article in short description
  * standards-version 3.9.3
    - debian/copyright: migrate to machine-readable format 1.0
  * debian/emacsen-setloadpath: use debian-pkg-add-load-path-item, instead
    of explicit setting of the load-path variable, to extend the load path
    (closes: #671557).

 -- Ralf Treinen <treinen@debian.org>  Sun, 06 May 2012 14:45:23 +0200

tuareg-mode (1:2.0.4-3) unstable; urgency=low

  * Really upload to sid.

 -- Ralf Treinen <treinen@debian.org>  Tue, 08 Feb 2011 22:47:36 +0100

tuareg-mode (1:2.0.4-2) experimental; urgency=low

  * Upload to sid.

 -- Ralf Treinen <treinen@debian.org>  Tue, 08 Feb 2011 21:49:55 +0100

tuareg-mode (1:2.0.4-1) experimental; urgency=low

  * New upstream version.
  * Byte-compile emacs files also on xemacs21, except for camldebug-tuareg.el

 -- Ralf Treinen <treinen@debian.org>  Thu, 18 Nov 2010 22:16:56 +0100

tuareg-mode (1:2.0.2-1) experimental; urgency=low

  * New upstream version.
  * Removed patch 0003-Narrow-autoload-pattern-down-to-what-is-really-neede
    since integrated by upstream.
  * Standards-version 3.9.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 22 Sep 2010 09:03:23 +0200

tuareg-mode (1:2.0.1-1) unstable; urgency=low

  * New upstream version. This version fixes a bug with byte-compilation
    under emacs21 and emacs22.

 -- Ralf Treinen <treinen@debian.org>  Thu, 27 May 2010 07:54:23 +0200

tuareg-mode (1:2.0.0-2) unstable; urgency=low

  [ Stéphane Glondu ]
  * New upstream location, update debian/{watch,control,copyright}
    accordingly

  [ Ralf Treinen ]
  * Do not byte-compile on emacs21, emacs22, and xemacs21. This fixes the
    urgency of bug #582981, though that bug remains open.

 -- Ralf Treinen <treinen@debian.org>  Tue, 25 May 2010 20:06:00 +0200

tuareg-mode (1:2.0.0-1) unstable; urgency=low

  [ Stéphane Glondu ]
  * New upstream release
    - update debian/copyright and debian/watch
  * Switch packaging to git
  * Switch debian/rules to dh with overrides
  * Switch source package format to 3.0 (quilt)
  * debian/control:
    - add myself to Uploaders, remove Remi and Stefano
    - update Homepage
    - bump Standards-Version to 3.8.4

  [ Ralf Treinen ]
  * changed section to ocaml

 -- Stéphane Glondu <glondu@debian.org>  Mon, 24 May 2010 19:54:44 +0200

tuareg-mode (1:1.45.6-2) unstable; urgency=low

  [ Ralf Treinen ]
  * Standards-Version 3.8.0 (no change)
  * Cleanup Uploaders field
  * debian/copyright: point to GPL-2 file.

  [ Mehdi Dogguy ]
  * tuareg-mode now recommands/enhances ocaml-interp instead of ocaml

 -- Ralf Treinen <treinen@debian.org>  Fri, 27 Feb 2009 22:08:12 +0100

tuareg-mode (1:1.45.6-1) unstable; urgency=low

  [ Ralf Treinen ]
  * New upstream release, fixes a bug concerning indentation of
    if-then-else (closes: Bug#450741), and elisp compiler warnings
    (closes: Bug#456006).
  * Standards-Version 3.7.3 (no change).

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

 -- Ralf Treinen <treinen@debian.org>  Sun, 13 Jan 2008 13:23:05 +0100

tuareg-mode (1:1.45.5-1) unstable; urgency=low

  * New upstream release.
  * debian/control: add Homepage field.
  * Proper end-of-string recognition in auto-mode file pattern. Thanks to
    Trent W. Buck for the patch (closes: Bug#446185).

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 Oct 2007 20:06:46 +0200

tuareg-mode (1:1.45.4-3) unstable; urgency=low

  * startup file: extend the load-path by the tuareg-mode directory

 -- Ralf Treinen <treinen@debian.org>  Mon, 21 May 2007 22:07:32 +0200

tuareg-mode (1:1.45.4-2) unstable; urgency=low

  * [Julien Cristau]
   - Remove inactive people from the Uploaders field.
  * [Ralf Treinen]
   - Recommends ocaml-mode, to profit from the added functionality of
     tuareg-mode-with-caml-mode. Added a note in the package description.
     (closes: Bug#414167).

 -- Ralf Treinen <treinen@debian.org>  Sat, 17 Mar 2007 19:16:39 +0100

tuareg-mode (1:1.45.4-1) unstable; urgency=low

  * Revert to upstream version 1.45 since indentation code in 1.46 is
    seriously broken (closes: Bug#389823).

 -- Ralf Treinen <treinen@debian.org>  Sun,  1 Oct 2006 19:11:35 +0200

tuareg-mode (1.46.1-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Tue, 29 Aug 2006 09:07:28 +0200

tuareg-mode (1.46.0-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version 3.7.2 (no change).
  * Turn Build-depends-indep: debhelper, dpatch into build-depends

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 May 2006 18:49:26 +0200

tuareg-mode (1.45.4-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Mon, 17 Apr 2006 00:04:14 +0200

tuareg-mode (1.45.3-1) unstable; urgency=low

  * New upstream release. This release fixes a bug due to erroneous
    detection of comments (closes: Bug#341203).

 -- Ralf Treinen <treinen@debian.org>  Fri, 17 Feb 2006 19:45:22 +0100

tuareg-mode (1.45.2-1) unstable; urgency=low

  * New upstream release. This release incorporates our patch
    to work around the missing line-number function
    in emacs (closes: #341203).
  * Install an emacs startup file with priority 51, and replace the installed
    append-tuareg.el file by an empty file. Adapt README.Debian accordingly,
    make a note in NEWS.
  * Some improvements in long package description.
  * Patch 03_autoload_pattern: Narrow autoload-pattern down to what is really
    needed for OCaml files.
  * New uploader Sylvain Le Gall.

 -- Ralf Treinen <treinen@debian.org>  Wed, 28 Dec 2005 15:46:08 +0100

tuareg-mode (1.45.0-beta3-1) unstable; urgency=low

  * New upstream release (called 1.45.0-rc3 by upstream).
  * Removed patch 03_keyword-face since fixed by upstream.
  * Standards-version 3.6.2 (no change).

 -- Ralf Treinen <treinen@debian.org>  Sat, 30 Jul 2005 09:38:18 +0200

tuareg-mode (1.45.0-beta2-1) unstable; urgency=low

  * New upstream version (called 1.45.0-rc2 by upstream).
  * Removed patches 03_fontlock and 04_charsetp since now fixed by
    upstream.
  * Patch 03_keyword-face: Fix bug on keyword coloration - thanks to Remi
    Vanicat for the patch (closes: #312212).
  * Added new uploader Samuel Mimram.

 -- Ralf Treinen <treinen@debian.org>  Tue,  7 Jun 2005 21:00:43 +0200

tuareg-mode (1.45.0-beta1-2) unstable; urgency=low

  * Patch 04_charsetp: Fixes compilation error under xemacs21-nomule due
    to unbound symbol charsetp. Thanks to Meik Hellmund for the patch.
    (closes: Bug#311274).

 -- Ralf Treinen <treinen@debian.org>  Wed,  1 Jun 2005 22:05:47 +0200

tuareg-mode (1.45.0-beta1-1) unstable; urgency=low

  * New upstream version (called 1.45.0-rc1 by upstream)
  * Corrected variable settings in patch 03_fontlock - thanks to Remi Vanicat
    (closes: Bug# 311155).
  * Removed patch 04_version since obsolete for this upstream version.

 -- Ralf Treinen <treinen@debian.org>  Sun, 29 May 2005 20:06:34 +0200

tuareg-mode (1.44.3-2) unstable; urgency=low

  * Fixed an embarrassing typo in changelog entry of 1.44.3-1.

 -- Ralf Treinen <treinen@debian.org>  Sat, 19 Feb 2005 16:26:05 +0100

tuareg-mode (1.44.3-1) unstable; urgency=low

  * New upstream release
  * Some cleanup in emacsen-startup and emacs-install
  * emacsen-install: remove path.elc after compilation
  * Patch 03_fontlock: check binding of some font-lock variables to avoid
    compilation error on installation. Thanks to Jérôme Marant for his
    help.
  * Patch 04_version: fixed version number
  * copyright: distinguish copyright and licence.

 -- Ralf Treinen <treinen@debian.org>  Sat, 19 Feb 2005 15:45:25 +0100

tuareg-mode (1.43.2-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Wed, 29 Dec 2004 16:32:35 +0100

tuareg-mode (1.43.0-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Wed, 24 Nov 2004 00:32:29 +0100

tuareg-mode (1.42.1-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Sun, 14 Nov 2004 16:16:01 +0100

tuareg-mode (1.42.0-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Wed, 10 Nov 2004 20:35:40 +0100

tuareg-mode (1.41.5-2) not-released; urgency=low

  * Fixed watch file

 -- Ralf Treinen <treinen@debian.org>  Wed, 27 Oct 2004 08:57:07 +0200

tuareg-mode (1.41.5-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Fri, 23 Jul 2004 20:28:56 +0200

tuareg-mode (1.41.4-1) unstable; urgency=low

  * New upstream release.
  * Fixed wrong regexp in watch file, placed it into debian directory.
  * Set maintainer to "Debian Ocaml Maintaines", uploaders to the gang of 5.
  * Dpatchified:

 -- Ralf Treinen <treinen@debian.org>  Fri, 19 Mar 2004 20:14:45 +0100

tuareg-mode (1.41.3-1) unstable; urgency=low

  * New upstream release.
  * Added a debian/watch file.

 -- Ralf Treinen <treinen@debian.org>  Mon, 23 Feb 2004 19:25:54 +0100

tuareg-mode (1.41.2-1) unstable; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Sun, 18 Jan 2004 11:59:07 +0100

tuareg-mode (1.40.5.1-1) unstable; urgency=low

  * New upstream release (upstream still calls this 1.40.5). Adds a
    function tuareg-narrow-to-phrase, see NEWS.Debian (closes: Bug#214844).
  * Added a NEWS.Debian file.

 -- Ralf Treinen <treinen@debian.org>  Wed, 29 Oct 2003 09:17:33 +0100

tuareg-mode (1.40.5-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.6.1 [no change]
  * Use file debian/compat instead of variable DH_COMPAT

 -- Ralf Treinen <treinen@debian.org>  Thu,  9 Oct 2003 22:26:23 +0200

tuareg-mode (1.40.4-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version 3.5.8.
  * DH_COMPAT=4, build-depends on debhelper >= 4.0.
  * Simplified debian/rules (removed build_stamp since there is nothing to do).
  * Short description: removed full stop.
  * Removed some redundancy in long description.

 -- Ralf Treinen <treinen@debian.org>  Sun, 26 Jan 2003 18:40:57 +0100

tuareg-mode (1.40.3.1-1) unstable; urgency=low

  * New upstream release (upstream 1.40.3, as of 15 Nov 2002). This
    release patches append-tuareg to not use sym-lock on mule-enabled xemacs.
  * Dependency: Changed xemacs21 into xemacs21-nomule, since the mule
    enabled xemacs does not work correctly with sym-lock.
  * Standards-version: 3.7.0
  * debian/rules: remove configure target since we don't need it.
  * debian/rules: some cleanup in install target.
  * debian/rules: install LISEZMOI into /usr/share/doc/tuareg-mode.

 -- Ralf Treinen <treinen@debian.org>  Sat, 16 Nov 2002 12:04:57 +0100

tuareg-mode (1.40.3-1) unstable; urgency=low

  * New upstream release. This release fixes a bug with indentation and
    alignment of comments (closes: Bug#156549), a bug with colorization
    (closes: Bug#157040), a bug with tuareg-electric-rb (closes:
    Bug#158897).
  * Changed dependency to "xemacs21 | emacsen", since xemacs21 seems to
    work best with tuareg-mode.
  * Added "Recommends: ocaml".

 -- Ralf Treinen <treinen@debian.org>  Thu, 10 Oct 2002 22:35:50 +0200

tuareg-mode (1.40.2-1) unstable; urgency=low

  * New upstream release. This release resolves a problem with "just in
    time fontification" (closes: Bug#154746) and a problem with the
    identation of comments (closes: Bug153909).

 -- Ralf Treinen <treinen@debian.org>  Fri,  2 Aug 2002 21:24:40 +0200

tuareg-mode (1.40.1-1) unstable; urgency=low

  * New upstream release (closes: Bug#152381).

 -- Ralf Treinen <treinen@debian.org>  Mon, 15 Jul 2002 13:18:32 +0200

tuareg-mode (1.40final-1) unstable; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Wed, 26 Jun 2002 21:22:25 +0200

tuareg-mode (1.40b-1) unstable; urgency=low

  * New upstream release.
  * Fix url of the locally installed ocaml doc.
  * Added emacsen-startup file to adjust the load-path for people
    who have not enabled search in subdirectories (closes: Bug#149189).

 -- Ralf Treinen <treinen@debian.org>  Fri,  7 Jun 2002 00:18:46 +0200

tuareg-mode (1.38.9-3) unstable; urgency=low

  * replaced in tuareg.el the definition of cadar and cddr by (require 'cl)
    as suggested by Henning Niss <hniss@diku.dk> (closes: Bug#130530).
    Forwarded to upstream.

 -- Ralf Treinen <treinen@debian.org>  Thu, 31 Jan 2002 19:23:58 +0100

tuareg-mode (1.38.9-2) unstable; urgency=low

  * fixed a spelling error in description (closes: Bug#125440).
  * some cleanup in debian/rules.

 -- Ralf Treinen <treinen@debian.org>  Wed,  9 Jan 2002 20:03:24 +0100

tuareg-mode (1.38.9-1) unstable; urgency=low

  * New upstream release

 -- Ralf Treinen <treinen@debian.org>  Sun,  9 Dec 2001 13:30:03 +0100

tuareg-mode (1.38.8-1) unstable; urgency=low

  * New upstream release.
  * Upstream has solved the problem with the redefinition of M-BS in xemacs
    (closes: Bug#117601).
  * Changed the "Depends: emacs20|xemacs21" to "Depends: emacsen" in order to
    accomodate all past, present and future versions of (x)emacs
    (closes: Bug#118844).
  * Patched the setting of tuareg-manual-url such that local documentation
    in /usr/share/doc/ocaml-doc is used if possible.

 -- Ralf Treinen <treinen@debian.org>  Wed, 14 Nov 2001 20:46:56 +0100

tuareg-mode (1.38.7-1) unstable; urgency=low

  * Initial Release. tuareg-mode has now its own package after having been
    included in the ocaml-tools package until 1.1-2 (hence the Conflicts
    with ocaml-tools <= 1.1-2).
  * Modifications in files coming from upstream:
    - camldebug.el renamed to camldebug-tuareg.el since the former exists
      already in the ocaml package. Changed reference in append-tuareg.el
      accordingly.
    - changed the ocaml library path in tuareg.el from
      /usr/local/lib/ocaml to /usr/lib/ocaml as it should be for debian.

 -- Ralf Treinen <treinen@debian.org>  Fri, 19 Oct 2001 20:53:43 +0200
